//
const moment = require('moment')

function MOMENT_CONSTRUCTOR() {
    this.utc7 = () => {
        return moment.utc().utcOffset('+07:00').format("YYYY-MM-DD HH:mm:ss")
    }

    this.dateLast7days = () => {
        return moment.utc().subtract(7, 'days').utcOffset('+07:00').format("YYYY-MM-DD HH:mm:ss")

    }

    this.invoiceDateFormat = () => {
        return moment.utc().utcOffset('+07:00').format("YYYYMMDDHHmmss")
    }


    this.utcOffset = '+07:00'
}

module.exports = new MOMENT_CONSTRUCTOR()

// module.exports = {
//     utcOffset: '+07:00',
//     utcTime: new utcTime7()
// };