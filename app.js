const express = require('express');
const app = express();
const apiRoute = require('./routes/api');
const version = 'v1/'
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
// const sshtunnel = require('./database/ssh-tunnel-db')

app.get('/', (req, res) => {
    res.send('Hello Harian Topskor')
})

app.get('/api/'+version, (req, res) => {
    res.send('Hello Harian Topskor With Version')
})

app.use(bodyParser.urlencoded({
    extended: true,
    // limit: '50mb'
}));
app.use(bodyParser.json({
    // limit: '50mb'
}));


app.use('/api/'+version, apiRoute); //ex localhost:3002/api/merchant/get/list/1

// app.use(cors());
//
// app.all('*', function(req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "X-Requested-With");
//     next();
// });
app.use(cookieParser());
// app.listen('3002')
const PORT = process.env.PORT || 3002
app.listen(PORT, console.log(`Server started on port ${PORT}`))