const jwt = require('jsonwebtoken')
const config = require('../config')

module.exports = (req, res, next) => {
    try{
        // console.log(req.headers.authorization)
        const token = req.headers.authorization.split(' ')[1]
        req.userData = jwt.verify(token, config.jwtSecretKey)
        next();
    }catch (error) {
        let err = null
        if (error.message === 'jwt expired'){
            err = 'Token Expired'
        }else{
            err = 'Auth Failed'
        }
        return res.status(401).json({
            // message: 'auth Failed'
            message: err
        })
    }
}