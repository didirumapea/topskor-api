const nodemailer = require('nodemailer');
let handleBars = require('nodemailer-express-handlebars');
let basepath =  __dirname.replace('plugins', '');

// console.log(basepath+'public/templates')
// #region GENERATE PROCESS REKONSILE MERCHANT VOUCHER CODE
exports.SendEmail = (detail) => {
    // console.log(name, inv, payMethode, price, tipePaket, tglOrder)
    let layoutTemplate = 'mailing-bank'
    let transporter = nodemailer.createTransport({
        pool: true,
        maxMessages: 100000,
        rateDelta: 2000,
        rateLimit: 2000,
        host: 'smtp.gmail.com',
        port: 587,
        requireTLS: true,
        // host: 'smtp.zoho.com', //'163.53.192.219', //'163.53.192.219',
        // port: '465',
        // secureConnection: true, // true for 465, false for other ports
        secure: false,
        auth: {
            user: 'topskor.co.id@gmail.com',
            pass: 'hariantopskor12345'
            // user: 'didirumapea2@gmail.com',
            // pass: 'dewwyamelia1601'
        }
    });

    if (detail.provider === null){
        // console.log('its not bank payment')
        layoutTemplate = 'mailing-default'
    }

    let handlebarOptions = {
        viewEngine: {
            extName: '.htm',
            partialsDir: basepath+'public/templates',
            layoutsDir: basepath+'public/templates',
            defaultLayout: layoutTemplate+'.htm',
        },
        viewPath: basepath+'public/templates', // folder name
        extName: '.htm'
    };

    transporter.use('compile', handleBars(handlebarOptions
        // viewPath: basepath+'public/templates', // folder name
        // //extName: '.html'
        // extName: '.htm'
    ));

    let mailOptions = {
        from: 'Topskor Invoice'+' <didirumapea2@gmail.com>', // sender address ex : <test-sendemail@intercity.com>
        //from: '"Bukopin Card Center" <test-sendemail@intercity.com>', // sender address
        to: [detail.email],
        subject: 'Pesanan Anda #'+detail.inv+' - STATUS:'+detail.status, // Subject line
        priority : 'high',
        important: false,
        template: layoutTemplate, // html body | file name
        context: {
            name : detail.name,
            inv: detail.inv,
            payMethod: detail.pay_methode,
            price: detail.price,
            tipePaket: detail.paket_name,
            tglOrder: detail.tgl_order,
            provider: detail.provider,
            namaRek: detail.nama_rek,
            noRek: detail.no_rek
        },
        // attachments: [{   // filename and content type is derived from path
        //     path: cfg.assetPath+'rekonsile-merchant-voucher/'+filename
        // }]
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
            //   console.log(error);
            //   res.json({
            //     success: true,
            //     data: 'send email error'
            //   });

        } else {
            // console.log(info);
            console.log('Send email invoice to '+detail.email+' done!')
            // console.log(detail.email)
            // res.json({
            //   success: true,
            //   data: 'send email success'
            // });
            // res.json({
            //             success: true,
            //             msg: "send email data merchant rekosile berhasil!",
            //             data: data
            //         })
        }

    })
}