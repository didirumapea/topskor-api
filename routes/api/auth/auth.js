const express = require('express');
const router = express.Router();
const db = require('../../../database').db_etopskor; // as const knex = require('knex')(config);
const setupPaginator = require('knex-paginator');
const checkAuth = require('../../../middleware/check-auth')
const config = require('../../../config')
const date = require('../../../config/moment-date-format')
const jwt = require('jsonwebtoken')
const moment = require('moment')
setupPaginator(db);


router.post('/user/login',  (req, res) => {
    let email = req.body.email
    let password = req.body.password
    // console.log(moment('2019-03-05 00:00:00').format("YYYY-MM-DD"))
    db.select(
        'id',
        'name',
        'email',
        )
        .from('users')
        .where('email', '=', email)
        .then(data => {
            if (data.length > 0){
                db.select('id', 'name', 'email')
                    .from('users')
                    .where('email', '=', email)
                    .andWhere('password', '=', password)
                    .then(data2 => {
                        if (data2.length > 0){
                            db.select(
                                't1.id',
                                'name',
                                'email',
                                'image',
                                'start_date',
                                'end_date',
                                'status',
                                'provider',
                                'code'
                                )
                                .from('users as t1')
                                .leftJoin('t_order as t2', 't1.id', 't2.id_user')
                                .where('email', '=', email)
                                .andWhere('password', '=', password)
                                .orderBy('end_date', 'desc')
                                .limit(1)
                                // .paginate(limit, page, true)
                                .then(data => {
                                    // console.log(data[0].name)
                                    // Mock User
                                    const user = {
                                        id: data[0].id,
                                        name: data[0].name,
                                        email: data[0].email,
                                        image: data[0].image,
                                        startDate: moment(data[0].start_date).format("YYYY-MM-DD"),
                                        endDate: moment(data[0].end_date).format("YYYY-MM-DD"),
                                        status: data[0].status,
                                        provider: data[0].provider,
                                        code: data[0].code
                                    }
                                    jwt.sign(user, config.jwtSecretKey, {expiresIn: '1d'}, (err, token) => {
                                        user.token = token
                                        res.json({
                                            success: true,
                                            message: "Login sukses",
                                            count: data.length,
                                            data: user
                                        });
                                    });

                                });
                        }else{
                            res.json({
                                success: false,
                                message: "Password salah",
                            });
                        }
                    });
            }else {
                console.log(data)
                res.json({
                    success: false,
                    message: "Email salah",
                    // data: data,
                })
            }
        })
        .catch((err) => {
            console.log('ERROR ON /user/login :'+err)
            res.json({
                success: false,
                message: err,
                // data: data,
            })
        })

});

router.post('/user/login-social',  (req, res) => {
    let name = req.body.name
    let email = req.body.email
    let image = req.body.image
    let provider = req.body.provider
    let provider_id = req.body.provider_id

    // EMAIL CHECKER
    db.select(
        't1.id',
        'name',
        'email',
        'image',
        'start_date',
        'end_date',
        'status',
        'provider',
        'code'
    )
        .from('users as t1')
        .leftJoin('t_order as t2', 't1.id', 't2.id_user')
        .where('email', email)
        .orderBy('end_date', 'desc')
        .limit(1)
        // .paginate(limit, page, true)
        .then(data2 => {
            // console.log(data2)
            if (data2.length > 0){
                // Mock User
                if (data2[0].start_date === 'Invalid date' || data2[0].start_date === 'Invalid date'){
                    data2[0].start_date = '0000-00-00'
                    data2[0].end_date = '0000-00-00'
                }
                const user = {
                    id: data2[0].id,
                    name: data2[0].name,
                    email: data2[0].email,
                    image: data2[0].image,
                    startDate: moment(data2[0].start_date).format("YYYY-MM-DD"),
                    endDate: moment(data2[0].end_date).format("YYYY-MM-DD"),
                    status: data2[0].status,
                    provider: data2[0].provider,
                    code: data2[0].code
                }
                jwt.sign(user, config.jwtSecretKey, {expiresIn: '1d'}, (err, token) => {
                    user.token = token
                    res.json({
                        success: true,
                        message: "Login social with email succees",
                        count: data2.length,
                        data: user
                    });
                });
                // PROVIDER CHECKER
            }else{
                db.select(
                    'id'
                )
                    .from('users')
                    .where('provider', provider)
                    .andWhere('provider_id', provider_id)
                    .then((data) => {
                        if (data.length > 0){
                            db.select(
                                't1.id',
                                'name',
                                'email',
                                'image',
                                'start_date',
                                'end_date',
                                'status',
                                'remember_token',
                                'provider',
                                'code'
                            )
                                .from('users as t1')
                                .leftJoin('t_order as t2', 't1.id', 't2.id_user')
                                .where('t1.id', '=', data[0].id)
                                .orderBy('end_date', 'desc')
                                .limit(1)
                                .then(data3 => {
                                    // console.log(data)
                                    if (data3.length > 0){
                                        // Mock User
                                        const user = {
                                            id: data3[0].id,
                                            name: data3[0].name,
                                            email: data3[0].email,
                                            image: data3[0].image,
                                            startDate: data3[0].start_date,
                                            endDate: data3[0].end_date,
                                            status: data3[0].status,
                                            provider: data3[0].provider,
                                            code: data3[0].code
                                        }
                                        jwt.sign(user, config.jwtSecretKey, {expiresIn: '1d'}, (err, token) => {
                                            user.token = token
                                            res.json({
                                                success: true,
                                                message: "Login social with provider & provider id sukses",
                                                count: data3.length,
                                                data: user
                                            });
                                        });
                                    }else {
                                        res.json({
                                            success: false,
                                            message: "something error",
                                            // count: data.length,
                                            // data: user
                                        });
                                    }

                                });
                        }else{
                            db('users')
                                .insert({
                                    name: name,
                                    email: email,
                                    image: image,
                                    provider: provider,
                                    provider_id: provider_id,
                                    created_at: date.utc7(),
                                    updated_at: date.utc7()
                                })
                                .then(data2 => {
                                    // console.log(data2[0])
                                    db.select(
                                        't1.id',
                                        'name',
                                        'email',
                                        'image',
                                        'start_date',
                                        'end_date',
                                        'status',
                                        'remember_token',
                                        'provider',
                                        'code'
                                    )
                                        .from('users as t1')
                                        .leftJoin('t_order as t2', 't1.id', 't2.id_user')
                                        .where('t1.id', '=', data2[0])
                                        .orderBy('end_date', 'desc')
                                        .limit(1)
                                        .then(data3 => {
                                            // console.log(data[0].name)
                                            // Mock User
                                            const user = {
                                                id: data3[0].id,
                                                name: data3[0].name,
                                                email: data3[0].email,
                                                image: data3[0].image,
                                                startDate: data3[0].start_date,
                                                endDate: data3[0].end_date,
                                                status: data3[0].status,
                                                provider: data3[0].provider,
                                                code: data3[0].code
                                            }
                                            jwt.sign(user, config.jwtSecretKey, {expiresIn: '1d'}, (err, token) => {
                                                // jwt.sign({user}, secretKey, {expiresIn: '30s'}, (err, token) => {
                                                user.token = token
                                                res.json({
                                                    success: true,
                                                    message: "Daftar social berhasil dan login social sukses",
                                                    count: data3.length,
                                                    data: user
                                                });
                                            });

                                        });
                                });

                        }

                    })
            }
        })
        .catch((err) => {

            console.log('ERROR ON /user/login-social : '+err)
            res.json({
                success: false,
                message: err,
                // data: data,
            })
        });





});

router.post('/user/register', (req, res) => {
    let name = req.body.name;
    let email = req.body.email
    let password = req.body.password
    let provider = req.body.provider
    // console.log(req.body)
    db.select(
        'id',
        'name',
        'email',
    )
        .from('users')
        .where('email', '=', email)
        .then(data => {
            if (data.length === 0){
                db('users')
                    .insert({
                        name: name,
                        email: email,
                        password: password,
                        provider: provider,
                        created_at: date.utc7(),
                        updated_at: date.utc7()
                    })
                    .then(data2 => {
                        // console.log(data2[0])
                        db.select(
                            't1.id',
                            'name',
                            'email',
                            'image',
                            'start_date',
                            'end_date',
                            'status',
                            'remember_token',
                            'provider',
                            'code'
                        )
                            .from('users as t1')
                            .leftJoin('t_order as t2', 't1.id', 't2.id_user')
                            .where('t1.id', '=', data2[0])
                            // .andWhere('password', '=', password)
                            .orderBy('end_date', 'desc')
                            .limit(1)
                            // .paginate(limit, page, true)
                            .then(data3 => {
                                // console.log(data[0].name)
                                // Mock User
                                const user = {
                                    id: data3[0].id,
                                    name: data3[0].name,
                                    email: data3[0].email,
                                    image: data3[0].image,
                                    startDate: data3[0].start_date,
                                    endDate: data3[0].end_date,
                                    status: data3[0].status,
                                    provider: data3[0].provider,
                                    code: data3[0].code
                                }
                                jwt.sign({user}, config.jwtSecretKey, {expiresIn: '1d'}, (err, token) => {
                                    // jwt.sign({user}, secretKey, {expiresIn: '30s'}, (err, token) => {
                                    user.token = token
                                    res.json({
                                        success: true,
                                        message: "Daftar berhasil dan login sukses",
                                        count: data3.length,
                                        data: user
                                    });
                                });

                            });
                    });
            }else {
                // console.log(data)
                res.json({
                    success: false,
                    message: "Email sudah terdaftar",
                    // data: data,
                })
            }
        })
        .catch((err) => {
            console.log('ERROR ON /user/register : '+err)
            res.json({
                success: false,
                message: err,
                // data: data,
            })
        })

});

router.get('/get/user', checkAuth,  (req, res) => {
    db.select(
        't1.id',
        'name',
        'email',
        'image',
        'start_date',
        'end_date',
        'status',
        'provider',
        'code'
        )
        .from('users as t1')
        .leftJoin('t_order as t2', 't1.id', 't2.id_user')
        .where('t1.id', '=', req.userData.id)
        .orderBy('end_date', 'desc')
        .limit(1)
        .then(data => {
            if (data.length === 0){
                res.json({
                    success: false,
                    message: "Data tidak di temukan",
                    count: data.length,
                    data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Sukses ambil data user",
                    count: data.length,
                    data: data,
                });
            }
        })
        .catch((err) => {
            console.log('ERROR ON /get/user\' : '+err)
            res.json({
                success: false,
                message: err,
                // data: data,
            })
        });

});

module.exports = router;