const express = require('express');
const router = express.Router();
const db = require('../../../database').db_etopskor; // as const knex = require('knex')(config);
const setupPaginator = require('knex-paginator');
const checkAuth = require('../../../middleware/check-auth')
const moment = require('moment')
setupPaginator(db);

// GET TRANSAKSI USER
router.get('/get/transaksi', checkAuth, (req, res) => {
    // console.log(req.userData)
    let id_user = req.userData.id
    // let remember_token = req.body.remember_token
    db.select(
        'no_invoice',
        'pay_methode',
        'payment_methode',
        'tipe_paket',
        'tgl_order',
        'start_date',
        'end_date',
        'status',
        'total_harga'
    )
        .from('t_order')
        .where('id_user', '=', id_user)
        // .orderBy('position', sortBy)
        // .paginate(limit, page, true)
        .then(data => {
            if (data.length === 0){
                res.json({
                    success: false,
                    message: "Data tidak di temukan",
                    count: data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Sukses ambil data user",
                    count: data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: data,
                });
            }
        })
        .catch((err) => {
            console.log('ERROR ON /get/transaksi : '+err)
            res.json({
                success: false,
                message: err,
            });
        });

});
// GET COUPON
router.get('/coupon/code=:code', checkAuth, (req, res) => {
    // let startDate
    // console.log(moment().isBetween('2019-09-11', '2019-09-15', "days", true))
    let code = req.params.code
    // let remember_token = req.body.remember_token
    db.select('*'
        // 'no_invoice',
        // // 'pay_methode',
        // // 'payment_methode',
        // // 'tipe_paket',
        // // 'tgl_order',
        // // 'start_date',
        // // 'end_date',
        // // 'status',
        // // 'total_harga'
    )
        .from('t_coupon')
        .where('coupon', code)
        // .orderBy('position', sortBy)
        // .paginate(limit, page, true)
        .then(data => {
            if (data.length === 0){
                res.json({
                    success: false,
                    message: "Coupon tidak di temukan",
                    count: data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: data,
                });
            }else{
                // console.log(data[0].usage, data[0].limit, data[0].start_date, data[0].end_date)
                let isBetweenDate = moment().isBetween(data[0].start_date, data[0].end_date, "days", true)
                // console.log(data[0].usage, data[0].limit, isBetweenDate)
                if (data[0].usage >= data[0].limit || !isBetweenDate){
                    res.json({
                        success: false,
                        message: "Kupon sudah melebihi batas limit atau tanggal yang telah di tentukan",
                        count: data.length,
                    });

                }else{
                    db('t_coupon')
                        .increment('usage', 1)
                        // .update({
                        //     usage: 2
                        // })
                        .where('coupon', code)
                        .then((data2) => {
                            res.json({
                                success: true,
                                message: "Sukses ambil data coupon",
                                count: data.length,
                                data: data,
                            });
                        })
                }

            }
        })
        .catch((err) => {
            console.log('ERROR ON /coupon/code=:code : '+err)
            res.json({
                success: false,
                message: err,
            });
        });
    // res.json({
    //     message: 'Post Created',
    //     authData
    // })

});

router.post('/code', checkAuth, (req, res) => {
    let code = req.body.code
    db('users')
        .where('id', req.userData.id)
        .update({
        code: code
    })
        .then(data => {
            // console.log(data)
            res.json({
                success: true,
                message: "update code berhasil.",
                data: data,
            });
        })
        .catch((err => {
            console.log('ERROR ON /code : '+err)
            res.json({
                success: false,
                message: err,
            });

        }));
});

module.exports = router;