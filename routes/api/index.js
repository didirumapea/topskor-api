const express = require('express');
const router = express.Router();
const checkAuth = require('../../middleware/check-auth')
const eTopskorRoute = require('./etopskor/etopskor');
const beritaRoute = require('./kitadisini/berita');
const auth = require('./auth/auth');
const user = require('./user/user');

// router.use('/merchant', todoRoute);
router.use('/etopskor', eTopskorRoute);
router.use('/berita', beritaRoute);
router.use('/auth', auth);
router.use('/user', checkAuth, user);

module.exports = router;