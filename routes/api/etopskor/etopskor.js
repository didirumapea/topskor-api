const express = require('express');
const router = express.Router();
const db = require('../../../database').db_etopskor; // as const knex = require('knex')(config);
const setupPaginator = require('knex-paginator');
const date = require('../../../config/moment-date-format')
const checkAuth = require('../../../middleware/check-auth')
const mailing = require('../../../plugins/mailing')
const currencyFormat = require('../../../plugins/currency-format')
const moment = require('moment')
setupPaginator(db);

// console.log(currencyFormat.currencyFormat(15000, 'Rp '))

// console.log(moment('2019-08-17 09:51:40').format("DD MMMM YYYY"))
// console.log(date.invoiceDateFormat())
//
// GET ADS
router.get('/get/ads/page=:page/limit=:limit/sort_by=:sort', (req, res) => {
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;

    // console.debug(req.params.page);
    // console.log(req.params.limit)
    // console.log(req.params.sort)
    db.select(
        '*'
    )
        .from('t_ads')
        // .where('publish', '=', 'Y')
        // .andWhere('parent_id', '=', '0')
        // .andWhere('tgl_pub', '<=', date.utc7())
        .orderBy('id', sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            res.json({
                success: true,
                message: "sukses ambil data",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                sortBy: sortBy,
                data: paginator.data,
            });
            // console.log(paginator.current_page);
            // console.log(paginator.data);
        })
        .catch((err) => {
            console.log('ERROR ON /get/ads/page=:page/limit=:limit/sort_by=:sort : '+err)
            res.json({
                success: false,
                message: err,
            });
        });
});
// GET EPAPER
router.get('/get/epaper/page=:page/limit=:limit/sort_by=:sort', (req, res) => {
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;

    // console.debug(req.params.page);
    // console.log(req.params.limit)
    // console.log(req.params.sort)
    db.select(
        'judul',
        'edisi',
        'image',
        'tgl_pub',
        'pdf'
        )
        .from('t_epaper')
        .where('publish', '=', 'Y')
        .andWhere('parent_id', '=', '0')
        .andWhere('tgl_pub', '<=', date.utc7())
        .orderBy('tgl_pub', sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            res.json({
                success: true,
                message: "sukses ambil data",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                sortBy: sortBy,
                data: paginator.data,
            });
            // console.log(paginator.current_page);
            // console.log(paginator.data);
        })
        .catch((err) => {
            console.log('ERROR ON /get/epaper/page=:page/limit=:limit/sort_by=:sort : '+err)
            res.json({
                success: false,
                message: err,
            });
        });
});
// GET DETAILS EPAPER
router.get('/get/epaper/details/tglpub=:tgl_pub/id_user=:id_user', (req, res) => {
    // let page = req.params.page;
    // let limit = req.params.limit;
    // let sortBy = req.params.sort;
    let edisi = req.params.edisi;
    let id_user = req.params.id_user
    let tgl_pub = req.params.tgl_pub
    if (id_user === 'null'){
        db.select(
            't1.edisi',
            't2.pdf'
        )
            .from('t_epaper as t1')
            .innerJoin('t_edisi as t2', 't1.edisi', 't2.id')
            // .where('edisi', '=', edisi)
            .where('tgl_pub', '<=', date.utc7())
            .groupBy('t1.edisi')
            // .orderBy('position', sortBy)
            // .paginate(limit, page, true)
            .then(data => {
                res.json({
                    success: true,
                    message: "sukses ambil data",
                    count: data.length,
                    status: 'not login',
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: data,
                });
                // console.log(paginator.current_page);
                // console.log(paginator.data);
            })
            .catch((err) => {
                console.log('ERROR ON /get/epaper/details/tglpub=:tgl_pub/id_user=:id_user: '+err)
                res.json({
                    success: false,
                    message: err,
                });
            });
    }else{
        // RAW MODE
        db.raw(`select t2.id as edisi, pdf
                from t_order t1
                inner join t_edisi t2
                on t2.tgl_edisi = ?
                where id_user = ?
                and (? BETWEEN start_date and end_date)
                limit 10
                ;`, [tgl_pub, id_user, tgl_pub])
            .then(data => {
                res.json({
                    success: true,
                    message: "sukses ambil data",
                    count: data[0].length,
                    status: '',
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // s                ortBy: sortBy,
                    data: data[0],
                });
                // console.log(paginator.current_page);
                // console.log(paginator.data);
            });
        // NO RAW MODE
        // db.select(
        //     't2.id as edisi',
        //     'pdf'
        // )
        //     .from('t_order as t1')
        //     .innerJoin('t_edisi as t2', 't2.tgl_edisi', tgl_pub)
        //     .whereRaw('? BETWEEN start_date and end_date', [tgl_pub])
        //     .andWhere('id_user', '=', id_user)
        //     // .andWhereBetween(db.raw('? BETWEEN start_date and end_date', [tgl_pub]))
        //     // .andWhereBetween(tgl_pub, ['start_date', 'end_date'])
        //     // .groupBy('t1.edisi')
        //     // .orderBy('position', sortBy)
        //     // .paginate(limit, page, true)
        //     .then(data => {
        //         res.json({
        //             success: true,
        //             message: "sukses ambil data",
        //             count: data.length,
        //             // current_page: paginator.current_page,
        //             // limit: paginator.data.length,
        //             // sortBy: sortBy,
        //             data: data,
        //         });
        //         // console.log(paginator.current_page);
        //         // console.log(paginator.data);
        //     });
    }
});
// GET DETAIL EPAPER WITH JWT
router.get('/get/epaper/details/tglpub=:tgl_pub', (req, res) => {
    // console.log(req.userData)
    let tgl_pub = req.params.tgl_pub
    let id_user = req.userData.id

    if (id_user === 'null'){
        db.select(
            't1.edisi',
            't2.pdf'
        )
            .from('t_epaper as t1')
            .innerJoin('t_edisi as t2', 't1.edisi', 't2.id')
            // .where('edisi', '=', edisi)
            .where('tgl_pub', '<=', date.utc7())
            .groupBy('t1.edisi')
            // .orderBy('position', sortBy)
            // .paginate(limit, page, true)
            .then(data => {
                res.json({
                    success: true,
                    message: "sukses ambil data",
                    count: data.length,
                    status: 'not login',
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: data,
                });
                // console.log(paginator.current_page);
                // console.log(paginator.data);
            })
            .catch((err) => {
                console.log('ERROR ON /get/epaper/details/tglpub=:tgl_pub : '+err)
                res.json({
                    success: false,
                    message: err,
                });
            });
    }else{
        // RAW MODE
        db.raw(`select t2.id as edisi, pdf
                from t_order t1
                inner join t_edisi t2
                on t2.tgl_edisi = ?
                where id_user = ?
                and (? BETWEEN start_date and end_date)
                limit 10
                ;`, [tgl_pub, id_user, tgl_pub])
            .then(data => {
                res.json({
                    success: true,
                    message: "sukses ambil data",
                    count: data[0].length,
                    // status: req.token,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // s                ortBy: sortBy,
                    data: data[0],
                });
                // console.log(paginator.current_page);
                // console.log(paginator.data);
            })
            .catch((err) => {
                console.log('ERROR ON /get/epaper/details/tglpub=:tgl_pub : '+err)
                res.json({
                    success: false,
                    message: err,
                });
            });
    }

});
// GET EDISI
router.get('/get/epaper/details/edisi=:edisi', (req, res) => {
    // let page = req.params.page;
    // let limit = req.params.limit;
    // let sortBy = req.params.sort;
    let edisi = req.params.edisi;
    // let id_user = req.params.id_user
    // let tgl_pub = req.params.tgl_pub
    db.select(
        'id as edisi',
        'pdf'
    )
        .from('t_edisi')
        // .innerJoin('t_edisi as t2', 't1.edisi', 't2.id')
        .where('id', '=', edisi)
        // .where('tgl_pub', '<=', dateNow)
        // .groupBy('t1.edisi')
        // .orderBy('position', sortBy)
        // .paginate(limit, page, true)
        .then(data => {
            res.json({
                success: true,
                message: "sukses ambil data",
                count: data.length,
                // status: 'not login',
                // current_page: paginator.current_page,
                // limit: paginator.data.length,
                // sortBy: sortBy,
                data: data,
            });
            // console.log(paginator.current_page);
            // console.log(paginator.data);
        })
        .catch((err) => {
            console.log('ERROR ON /get/epaper/details/edisi=:edisi : '+err)
            res.json({
                success: false,
                message: err,
            });
        });
});
// POST ORDER
router.post('/user/order', checkAuth, (req, res) => {
    // console.log(req.userData.email)

    let order = {
        id_user: req.userData.id,
        no_invoice: req.body.no_invoice,
        pay_methode: req.body.pay_methode,
        payment_methode: req.body.payment_methode,
        tipe_paket: req.body.tipe_paket,
        tgl_order: req.body.tgl_order,
        start_date: req.body.start_date,
        end_date: req.body.end_date,
        status: req.body.status,
        affiliate: req.body.affiliate,
        affiliate_id: req.body.affiliate_id,
        coupon: req.body.coupon,
        id_karyawan_alfamart: req.body.id_karyawan_alfamart,
        total_harga: req.body.total_harga,
        source: req.body.source,
        upgrade: req.body.upgrade,
        txn_coda: req.body.txn_coda

    }
    console.log(order)
    db('t_order')
        .insert(order)
        .then(data => {
            db.select()
                .from('t_order as t1')
                .leftJoin('t_pay as t2', 't1.pay_methode', 't2.id')
                .leftJoin('t_payMethode as t3', 't1.payment_methode', 't3.id')
                .leftJoin('t_paket as t4', 't1.tipe_paket', 't4.id')
                .where('t1.id', data[0])
                .then((result) => {
                    // console.log(result[0])
                    mailing.SendEmail({
                        email: req.userData.email,
                        name: req.userData.name,
                        inv: result[0].no_invoice,
                        pay_methode: result[0].name_methode,
                        price: currencyFormat.currencyFormat(parseInt(result[0].total_harga), 'Rp '),
                        paket_name: result[0].nama_paket,
                        status: result[0].status.toUpperCase(),
                        tgl_order: moment(result[0].tgl_order).format("DD MMMM YYYY"),
                        no_rek: result[0].no_rek,
                        nama_rek: result[0].nama_rek,
                        provider: result[0].provider
                })
                    res.json({
                        success: true,
                        message: "Order berhasil.",
                        count: result.length,
                        data: result,
                    });
                })

        }).catch((err) =>{
            console.log('error at '+date.utc7())
            console.log(err)
        res.json({
            success: false,
            message: "Order failed.",
            // count: data.length,
            data: err,
        });
    });
});
// POST VERIFICATION ORDER
router.post('/user/order-verification', checkAuth, (req, res) =>  {

    let order = {
        no_invoice: req.body.no_invoice,
        start_date: req.body.start_date,
        end_date: req.body.end_date,
        txn_coda: req.body.txn_coda,
        status: req.body.status,

    }
    db('t_order')
        .where('no_invoice', order.no_invoice)
        .update({
            start_date: order.start_date,
            end_date: order.end_date,
            status: order.status,
            txn_coda: order.txn_coda
        })
        .then(data => {

            res.json({
                success: true,
                message: "Verifikasi berhasil.",
                count: data.length,
                data: data,
            });

        }).catch((err) =>{
        console.log(err)
        res.json({
            success: false,
            message: "Order failed.",
            // count: data.length,
            data: err,
        });
    });
});
// GET PAKET
router.get('/get/paket', (req, res) => {
    db.select()
        .from('t_paket')
        .then(data => {
            if (data.length === 0){
                res.json({
                    success: false,
                    message: "Data tidak di temukan",
                    count: data.length,
                    data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Sukses ambil data",
                    count: data.length,
                    data: data,
                });
            }
        })
        .catch((error => {
            console.log(error)
        }));
});

module.exports = router;