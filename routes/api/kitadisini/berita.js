const express = require('express');
const router = express.Router();
const db = require('../../../database').db_kitadisini; // as const knex = require('knex')(config);
const date = require('../../../config/moment-date-format')
const setupPaginator = require('knex-paginator');
setupPaginator(db);


// INTRO
router.get('/', (req, res) => {
    res.send('Hello Harian Topskor With Berita API');
});

// -------------------------------------- REGION DATABASE KITADISINI ----------------------------------------
// LATEST HOME
router.get('/get/list/home/latest/page=:page/limit=:limit/sort_by=:sort', (req, res) => {
    // console.log(date.utc7())

    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;
    db.select('t1.id_artikel', 't1.judul_artikel', 't1.subjudul', 't1.isi_artikel', 't1.thumbnail', 't1.tgl_pub', 't2.id_object2 as id_section')
        .from('t_artikel as t1')
        .innerJoin('t_relasi as t2', 't1.id_artikel', 't2.id_object')
        .where('t1.publish', '=', 'Y')
        .andWhere('t2.tipe', 'art_sec')
        .andWhere('tgl_pub', '<=', date.utc7())
        .orderBy('tgl_pub', sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            // console.log('C-Console : Sukses ambil data koran terbaru');
            res.json({
                success: true,
                message: "sukses ambil data",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                sortBy: sortBy,
                data: paginator.data,
            });
            // console.log(paginator.current_page);
            // console.log(paginator.data);
        });
});
// POPULAR HOME
router.get('/get/list/home/popular/page=:page/limit=:limit/sort_by=:sort', (req, res) => {
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;
    let lastDate = date.dateLast7days()
    let nowDate = date.utc7()
    // console.log(lastDate, nowDate)
    db.select('t1.id_artikel', 't1.judul_artikel', 't1.subjudul', 't1.isi_artikel', 't1.thumbnail', 't1.tgl_pub', 't2.id_object2 as id_section')
        .from('t_artikel as t1')
        .innerJoin('t_relasi as t2', 't1.id_artikel', 't2.id_object')
        .where('publish', '=', 'Y')
        .andWhere('t2.tipe', 'art_sec')
        .andWhereBetween('tgl_pub', [lastDate, nowDate])
        .orderBy('dibaca', sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            // console.log('C-Console : Sukses ambil data koran popular');
            res.json({
                success: true,
                message: "sukses ambil data",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                sortBy: sortBy,
                data: paginator.data,
            });
            // console.log(paginator.current_page);
            // console.log(paginator.data);
        });
});
// HOT NEWS HOME
router.get('/get/list/home/custom=:subsection_id/page=:page/limit=:limit/sort_by=:sort', (req, res) => {
    let supsectionId = req.params.subsection_id
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;
    db.select('t1.id_artikel', 't1.judul_artikel', 't1.subjudul', 't1.isi_artikel', 't1.thumbnail', 't1.tgl_pub', 't2.id_object2 as id_section')
        .from('t_artikel as t1')
        .innerJoin('t_relasi as t2', 't1.id_artikel', 't2.id_object')
        .innerJoin('t_section as t3', 't2.id_object2', 't3.id_section')
        .where('t1.publish', '=', 'Y')
        .andWhere('t2.tipe', 'art_sec')
        .andWhere('id_supsection', supsectionId)
        .andWhere('tgl_pub', '<=', date.utc7())
        .orderBy('tgl_pub', sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            res.json({
                success: true,
                message: "sukses ambil data",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                sortBy: sortBy,
                data: paginator.data,
            });
            // console.log(paginator.current_page);
            // console.log(paginator.data);
        });
});
// GET CATEGORY
router.get('/get/list/category', (req, res) => {

    db.select(
        't2.id_supsection',
        't2.id_section',
        'nama_supsection',
        't2.nama_section'
        )
        .from('t_supsection as t1')
        .innerJoin('t_section as t2', 't1.id_supsection', 't2.id_supsection')
        .where('t1.status', '=', '1')
        .andWhere('t2.status', '1')
        .orderBy('t2.id_supsection', 'asc')
        // .paginate(limit, page, true)
        .then(data => {
            let supsection = []
            let jsonObj = {}
            let jsonSection = {}
            let id_sup = null
            data.forEach(element => {
                if (element.id_supsection !== id_sup){
                    jsonObj = {}
                    jsonObj.id = element.id_supsection
                    jsonObj.supsection_name = element.nama_supsection
                    jsonObj.section = []
                    jsonSection.id = element.id_section
                    jsonSection.section_name = element.nama_section
                    jsonObj.section.push(jsonSection)
                    supsection.push(jsonObj)

                    // console.log('ini adalah '+element.nama_supsection)
                }else{
                    jsonSection = {}
                    jsonSection.id = element.id_section
                    jsonSection.section_name = element.nama_section
                    jsonObj.section.push(jsonSection)
                    // console.log(element.nama_section)
                }
                id_sup = element.id_supsection
            })

            res.json({
                success: true,
                message: "sukses ambil data category",
                // current_page: dataa.current_page,
                // limit: paginator.data.length,
                // sortBy: sortBy,
                data: supsection,
            });
            // console.log(paginator.current_page);
            // console.log(paginator.data);
        });
});
// GET CATEGORY BY ID
router.get('/get/list/category/section_id=:sec_id/page=:page/limit=:limit/sort_by=:sort', (req, res) => {
    let sec_id = req.params.sec_id
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;
    db.select('t1.id_artikel', 't1.judul_artikel', 't1.subjudul', 't1.isi_artikel', 't1.thumbnail', 't1.tgl_pub', 't2.id_object2 as id_section')
        .from('t_artikel as t1')
        .innerJoin('t_relasi as t2', 't1.id_artikel', 't2.id_object')
        .where('t1.publish', '=', 'Y')
        .andWhere('t2.tipe', 'art_sec')
        .andWhere('t2.id_object2', sec_id)
        .andWhere('tgl_pub', '<=', date.utc7())
        .orderBy('tgl_pub', sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            res.json({
                success: true,
                message: "sukses ambil data dengan id "+ sec_id,
                current_page: paginator.current_page,
                limit: paginator.data.length,
                sortBy: sortBy,
                data: paginator.data,
            });
            // console.log(paginator.current_page);
            // console.log(paginator.data);
        });
});
// GET SEARCH
router.get('/get/list/search/search=:val/page=:page/limit=:limit/sort_by=:sort', (req, res) => {
    let val = req.params.val
    let page = req.params.page;
    let limit = req.params.limit;
    let sortBy = req.params.sort;
    db.select('t1.id_artikel', 't1.judul_artikel', 't1.subjudul', 't1.isi_artikel', 't1.thumbnail', 't1.tgl_pub', 't2.id_object2 as id_section')
        .from('t_artikel as t1')
        .innerJoin('t_relasi as t2', 't1.id_artikel', 't2.id_object')
        .where('t1.publish', '=', 'Y')
        .andWhere('t2.tipe', 'art_sec')
        .andWhere('t1.judul_artikel', 'like', '%'+val+'%')
        .andWhere('tgl_pub', '<=', date.utc7())
        .orderBy('tgl_pub', sortBy)
        .paginate(limit, page, true)
        .then(paginator => {
            res.json({
                success: true,
                message: "sukses ambil data",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                sortBy: sortBy,
                data: paginator.data,
            });
            // console.log(paginator.current_page);
            // console.log(paginator.data);
        });
});

// -------------------------------------- END REGION DATABASE KITADISINI ----------------------------------------




module.exports = router;