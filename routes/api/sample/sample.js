const express = require('express');
const router = express.Router();
const db = require('../../../database').db_etopskor; // as const knex = require('knex')(config);
const setupPaginator = require('knex-paginator');
const checkAuth = require('../../../middleware/check-auth')
setupPaginator(db);

// ------------------------------------- SAMPLE -------------------------------------------

router.get('/get/list/:id_merchant', (req, res)=> {

    db.select('t2.promo_title',
        'inv_code',
        'voucher_code',
        'transaction_status',
        'name as customer_name',
        't1.quantity',
        'total'
    ).from('ecommerce_transaction as t1').innerJoin('merchant_promo as t2', 't1.fid_promo', 't2.id').where('t1.fid_merchant', req.params.id_merchant).orderBy('t1.id').then(function (data) {
        res.send(data);
        console.log(data.length)
    });
});

router.get('/get/city',  (req, res) => {
    console.log(db.pool)
    // db.select().from('city').orderBy('id').then(function (data) {
    res.json({
        success: true,
        message: "sukses ambil data",
        // data: data,
    });
    //     console.log(data)
    // });
});

router.post('/', (req, res) => {
    db.insert(req.body).returning('*').into('todo').then(function (data) {
        res.send(data);
    });
});

router.patch('/:id', (req, res) => {
    db('todo').where({ id: req.params.id }).update(req.body).returning('*').then(function (data) {
        res.send(data);
    });
});

router.put('/:id', (req, res) => {
    db('todo').where({ id: req.params.id }).update({
        title: req.body.title || null,
        is_done: req.body.is_done || null
    }).returning('*').then(function (data) {
        res.send(data);
    });
});

router.delete('/:id', (req, res) => {
    db('todo').where({ id: req.params.id }).del().then(function () {
        res.json({ success: true });
    });
});

router.get('/:id',(req, res) => {
    db('todo').where({id: req.params.id}).first().then(function(data) {
        res.send(data);
    });
});

router.post('/sample/jwt', checkAuth, (req, res) => {
    jwt.verify(req.token, process.env.JWT_KEY, (err, authData) => {
        if(err){
            res.sendStatus(403)
        }else{
            res.json({
                message: 'Post Created',
                authData
            })
        }
    })
})

router.post('/sample/jwt/v2', checkAuth, (req, res) => {
    res.json({
        message: 'Post Created',
        data: req.userData
    })
})

module.exports = router;