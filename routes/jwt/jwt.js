function JWT() {

    // FORMAT OF TOKEN
    // Authorization: Bearer <access token>

    // Verify Token
    this.verifyToken = (req, res, next) => {
        // Get auth header value
        const bearerHeader = req.headers['authorization']
        // console.log(req.headers['authorization'])
        // Check if bearer is undefined
        if (typeof bearerHeader !== 'undefined'){
            // split at the space
            const bearer = bearerHeader.split(' ');
            // get token from array
            const bearerToken = bearer[1]
            // set the token
            req.token = bearerToken
            next();
        }else{
            //Forbidden
            res.sendStatus(403);
        }
    }

    this.verifyToken2 = (req, res, next) => {
        // Get auth header value
        const bearerHeader = req.headers['authorization']

        // Check if bearer is undefined
        if (typeof bearerHeader !== 'undefined'){
            // split at the space
            const bearer = bearerHeader.split(' ');
            // get token from array
            // set the token
            req.token = bearer[1]
            next();
        }else{
            // console.log(req.headers['authorization'])
            // // split at the space
            // const bearer = bearerHeader.split(' ');
            // // get token from array
            // // set the token
            // req.token = bearer[1]
            // next();
            //Forbidden
            res.sendStatus(403);
        }
    }

}
module.exports = new JWT();